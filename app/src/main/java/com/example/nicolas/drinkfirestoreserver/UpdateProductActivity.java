package com.example.nicolas.drinkfirestoreserver;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nicolas.drinkfirestoreserver.Model.Category;
import com.example.nicolas.drinkfirestoreserver.Model.Drink;
import com.example.nicolas.drinkfirestoreserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkfirestoreserver.Utils.Common;
import com.example.nicolas.drinkfirestoreserver.Utils.ProgressRequestBody;
import com.example.nicolas.drinkfirestoreserver.Utils.UploadCallBack;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProductActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "UpdateProductActivity";
    private static final int PICK_FILE_REQUEST = 9999;

    ImageView browserImageView;
    EditText nameEditText, priceEditText, descriptionEditText;
    Button updateButton, deleteButton;

    IDrinkShopAPI mService;

    CompositeDisposable compositeDisposable;

    Uri selectedFileUri = null;
    String selected_category="";
    File compressedImageFile;

    MaterialSpinner menuSpinner;

    HashMap<String,String> menu_data_for_get_key = new HashMap<>();
    HashMap<String,String> menu_data_for_get_value = new HashMap<>();

    List<String> menu_data = new ArrayList<>();

    private FirebaseFirestore firebaseFirestore;
    private StorageReference storageReference;

    Uri downloadUri;

    int drinkSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);

        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();


        //View
        deleteButton = findViewById(R.id.delete_button);
        updateButton = findViewById(R.id.update_button);
        nameEditText = findViewById(R.id.drink_name_edit_text);
        priceEditText = findViewById(R.id.drink_price_edit_text);
        descriptionEditText = findViewById(R.id.drink_desc_edit_text);
        browserImageView = findViewById(R.id.browser_image_view);
        menuSpinner = findViewById(R.id.menu_id_spinner);

        //API
        mService = Common.getAPI();

        //RxJava
        compositeDisposable = new CompositeDisposable();

        if(Common.currentDrink != null)
            selected_category = Common.currentCategory.getMenuId();


        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        menuSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selected_category = menu_data_for_get_key.get(menu_data.get(position));
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct();
            }
        });

        setSpinnerMenu();
        setProductInfo();

    }

    private void setProductInfo() {
        if(Common.currentDrink != null){
            nameEditText.setText(Common.currentDrink.name);
            priceEditText.setText(String.valueOf(Common.currentDrink.price));
            descriptionEditText.setText(Common.currentDrink.description);
            Picasso.with(this).load(Common.currentDrink.link).into(browserImageView);
            menuSpinner.setSelectedIndex(menu_data.indexOf(menu_data_for_get_value.get(Common.currentCategory.getMenuId())));
        }
    }

    private void deleteProduct() {

        //delete product in firestore
        firebaseFirestore.collection("Menu/"+Common.currentCategory.getMenuId()+"/Drinks")
                .document(Common.currentDrink.getDrinkId())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                        Toast.makeText(UpdateProductActivity.this, "Drink has been deleted", Toast.LENGTH_SHORT).show();
                        selectedFileUri=null;
                        Common.currentDrink=null;
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });

        //todo.delete image on storage??

    }

    private void updateProduct() {

        if(selectedFileUri!=null){

            //Update name and link
            firebaseFirestore.collection("Menu/"+Common.currentCategory.getMenuId()+"/Drinks")
                    .document(Common.currentDrink.getDrinkId())
                    .update("name", nameEditText.getText().toString(),
                            "link", downloadUri.toString(),
                            "price", Float.parseFloat(priceEditText.getText().toString()),
                            "description", descriptionEditText.getText().toString()
                            )
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "DocumentSnapshot successfully updated!");
                            Toast.makeText(UpdateProductActivity.this, "Category updated!", Toast.LENGTH_SHORT).show();
                            selectedFileUri=null;
                            Common.currentDrink=null;
                            finish();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error updating document", e);
                            Toast.makeText(UpdateProductActivity.this, "Error de conexion", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {

            //Update only name (link remains with Common.currentCategory.getLink() )
            firebaseFirestore.collection("Menu/"+Common.currentCategory.getMenuId()+"/Drinks")
                    .document(Common.currentDrink.getDrinkId())
                    .update("name", nameEditText.getText().toString(),
                            "link", Common.currentDrink.getLink(),
                            "price", Float.parseFloat(priceEditText.getText().toString()),
                            "description", descriptionEditText.getText().toString()
                    )
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "DocumentSnapshot successfully updated!");
                            Toast.makeText(UpdateProductActivity.this, "Drink updated!", Toast.LENGTH_SHORT).show();
                            Common.currentDrink=null;
                            finish();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error updating document", e);
                            Toast.makeText(UpdateProductActivity.this, "Error de conexion", Toast.LENGTH_SHORT).show();
                        }
                    });

        }

        //Si se ha cambiado de categoria en el spinner
        if(!selected_category.equals(Common.currentCategory.getMenuId())){

            deleteProduct();

            //create Drink Map
            final Map<String, Object> drink = new HashMap<>();
            drink.put("name", Common.currentDrink.getName());
            drink.put("price", Common.currentDrink.getPrice());
            drink.put("link", Common.currentDrink.getLink());
            drink.put("description", Common.currentDrink.getDescription());

            //add current product to new Menu
            firebaseFirestore.collection("Menu/"+selected_category+"/Drinks")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                    drinkSize = task.getResult().size(); //get drinks size
                                }

                                //Set Menu to Firestore (using category size to make "autoincrement")
                                firebaseFirestore.collection("Menu/"+selected_category+"/Drinks")
                                        .document(String.valueOf(drinkSize+1))
                                        .set(drink)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(UpdateProductActivity.this, "Drink agregado a nuevo Menu", Toast.LENGTH_SHORT).show();
                                                selectedFileUri = null;
                                                finish();

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(UpdateProductActivity.this, "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }

                    });


        }

    }

    //Crtl+O
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                browserImageView.setImageURI(selectedFileUri);
                uploadFileToServer(selectedFileUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(UpdateProductActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(UpdateProductActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(UpdateProductActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(UpdateProductActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    private void uploadFileToServer(Uri selectedFileUri) {

        if(selectedFileUri != null){

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(UpdateProductActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = UUID.randomUUID().toString();

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/product/"+fileName+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Toast.makeText(UpdateProductActivity.this, "Upload failed."+ exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(UpdateProductActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(UpdateProductActivity.this, "Upload: " + taskSnapshot.getBytesTransferred()*0.01, Toast.LENGTH_SHORT).show();
                }
            });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);



                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });

        }
    }

    private void setSpinnerMenu() {
        for(Category category : Common.menuList){ //Common.menuList lo seteamos = categories en HomeActivity
            menu_data_for_get_key.put(category.getName(),category.getMenuId());
            menu_data_for_get_value.put(category.getMenuId(),category.getName());

            menu_data.add(category.getName());

            menuSpinner.setItems(menu_data);
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
