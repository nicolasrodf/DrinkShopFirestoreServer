package com.example.nicolas.drinkfirestoreserver.Model;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

public class Category{

    public int id;
    public String name;
    public String link;

    @Exclude
    public String menuId;

    public Category() {
    }

    public Category(String name, String link) {
        this.name = name;
        this.link = link;
    }

    public <T extends Category> T withId(@NonNull final String id) {
        this.menuId = id;
        return (T) this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

