package com.example.nicolas.drinkfirestoreserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nicolas.drinkfirestoreserver.Adapter.DrinkListAdapter;
import com.example.nicolas.drinkfirestoreserver.Model.Category;
import com.example.nicolas.drinkfirestoreserver.Model.Drink;
import com.example.nicolas.drinkfirestoreserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkfirestoreserver.Utils.Common;
import com.example.nicolas.drinkfirestoreserver.Utils.ProgressRequestBody;
import com.example.nicolas.drinkfirestoreserver.Utils.UploadCallBack;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DrinkListActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "DrinkListActivity";

    private static final int PICK_FILE_REQUEST = 9998;

    IDrinkShopAPI mService;
    RecyclerView drinksRecycler;

    FloatingActionButton addButton;

    ImageView browserImageView;
    EditText drinkNameEditText, drinkPriceEditText, descriptionEditText;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    Uri selectedFileUri = null;
    String uploadedImgPath = "";
    File compressedImageFile;

    private FirebaseFirestore firebaseFirestore;
    private StorageReference storageReference;

    int drinkSize;

    Uri downloadUri;

    int lastKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_list);

        mService = Common.getAPI();

        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDrinkDialog();
            }
        });

        drinksRecycler = findViewById(R.id.drinks_recycler);
        drinksRecycler.setLayoutManager(new GridLayoutManager(this,2));
        drinksRecycler.setHasFixedSize(true);

        loadDrinkList(Common.currentCategory.getMenuId());

        getLastKey();
    }

    private void getLastKey() {
        Query query = firebaseFirestore.collection("Menu/"+Common.currentCategory.getMenuId()+"/Drinks")
                .orderBy("id", Query.Direction.DESCENDING).limit(1);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d(TAG, document.getId() + " => " + document.getData());
                        Category category = document.toObject(Category.class);
                        lastKey = category.getId();
                        Toast.makeText(DrinkListActivity.this, "LAST KEY " + lastKey, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "Error getting documents: ", task.getException());
                }
            }
        });
    }

    private void showAddDrinkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar Nuevo Producto");

        View view = LayoutInflater.from(this).inflate(R.layout.add_new_product_layout,null);

        drinkNameEditText = view.findViewById(R.id.drink_name_edit_text);
        drinkPriceEditText = view.findViewById(R.id.drink_price_edit_text);
        descriptionEditText = view.findViewById(R.id.drink_desc_edit_text);
        browserImageView = view.findViewById(R.id.browser_image_view);

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        //Set View
        builder.setView(view);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                uploadedImgPath = "";
                selectedFileUri = null;
            }
        }).setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(drinkNameEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese el nombre del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(drinkPriceEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese el precio del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(descriptionEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese la descripcion del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(downloadUri.toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor seleccione la imagen del producto", Toast.LENGTH_SHORT).show();
                    return;
                }


                final Map<String, Object> drink = new HashMap<>();
                drink.put("id", lastKey+1);
                drink.put("name", drinkNameEditText.getText().toString());
                drink.put("price", Float.parseFloat(drinkPriceEditText.getText().toString()));
                drink.put("link", downloadUri.toString());
                drink.put("description", descriptionEditText.getText().toString());


                //Set Menu to Firestore (using category size to make "autoincrement")
                firebaseFirestore.collection("Menu/"+Common.currentCategory.getMenuId()+"/Drinks")
                        .document(String.valueOf(lastKey+1))
                        .set(drink)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(DrinkListActivity.this, "Drink agregado", Toast.LENGTH_SHORT).show();
                                loadDrinkList(Common.currentCategory.getMenuId()); //refresh page
                                selectedFileUri = null;

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(DrinkListActivity.this, "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


            }
        }).show();

    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(DrinkListActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(DrinkListActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(DrinkListActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                browserImageView.setImageURI(selectedFileUri);
                uploadFileToServer(selectedFileUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(DrinkListActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    private void uploadFileToServer(Uri selectedFileUri) {

        if(selectedFileUri != null){

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(DrinkListActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = UUID.randomUUID().toString();

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/product/"+fileName+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Toast.makeText(DrinkListActivity.this, "Upload failed."+ exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(DrinkListActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(DrinkListActivity.this, "Upload: " + taskSnapshot.getBytesTransferred()*0.01, Toast.LENGTH_SHORT).show();
                }
            });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);
                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });

        }
    }

    private void loadDrinkList(String menuId) {

        final List<Drink> drinks = new ArrayList<>();

        firebaseFirestore.collection("Menu/" + menuId + "/Drinks")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                //Toast.makeText(HomeActivity.this, "name: " + document.get("name"), Toast.LENGTH_SHORT).show();
                                Drink drink = document.toObject(Drink.class).withId(document.getId());
                                drinks.add(drink);

                            }

                            displayDrinkList(drinks);

                        }

                    }

                });
    }

    private void displayDrinkList(List<Drink> drinks) {
        Toast.makeText(this, "SIZE DRINK LIST " +drinks.size(), Toast.LENGTH_SHORT).show();
        DrinkListAdapter adapter = new DrinkListAdapter(this,drinks);
        drinksRecycler.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        loadDrinkList(Common.currentCategory.getMenuId());
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
