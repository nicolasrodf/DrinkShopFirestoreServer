package com.example.nicolas.drinkfirestoreserver;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nicolas.drinkfirestoreserver.Adapter.OrderDetailAdapter;
import com.example.nicolas.drinkfirestoreserver.Adapter.OrderViewAdapter;
import com.example.nicolas.drinkfirestoreserver.Model.Cart;
import com.example.nicolas.drinkfirestoreserver.Model.DataMessage;
import com.example.nicolas.drinkfirestoreserver.Model.MyResponse;
import com.example.nicolas.drinkfirestoreserver.Model.Order;
import com.example.nicolas.drinkfirestoreserver.Model.Token;
import com.example.nicolas.drinkfirestoreserver.Model.User;
import com.example.nicolas.drinkfirestoreserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkfirestoreserver.Retrofit.IFCMService;
import com.example.nicolas.drinkfirestoreserver.Utils.Common;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderDetailActivity extends AppCompatActivity {
    private static final String TAG = "ViewOrderDetailActivity";

    TextView txt_order_id, txt_name, txt_phone, txt_order_price, txt_order_address, txt_order_comment;
    Spinner orderStatusSpinner;

    RecyclerView orderRecyclerView;

    //Declare values for spinner
    String[] spinnerSource = new String[]{
            "Cancelado", //index 0
            "Solicitado", //1
            "En Proceso", //2
            "En Camino", //3
            "Finalizado", //4
    };

    IDrinkShopAPI mService;
    IFCMService mFCMService;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    String name;

    ImageView messageImageView, phoneImageView;

    FirebaseFirestore firebaseFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        setTitle("READY BEBIDAS");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firebaseFirestore = FirebaseFirestore.getInstance();

        mService = Common.getAPI();
        mFCMService = Common.getFCMApi();

        //Send whatsapp msg
        messageImageView = findViewById(R.id.msg_image_view);
        messageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            }
        });

        //Make phonecall
        phoneImageView = findViewById(R.id.phone_image_view);
        phoneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ViewOrderDetailActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", Common.currentOrder.getUserPhone(), null));
                startActivity(intent);
            }
        });

        txt_order_id = findViewById(R.id.order_id_text_view);
        txt_name = findViewById(R.id.name_text_view);
        txt_phone = findViewById(R.id.phone_text_view);
        txt_order_price = findViewById(R.id.order_price_text_view);
        txt_order_address = findViewById(R.id.order_address_text_view);
        txt_order_comment = findViewById(R.id.order_comment_text_view);
        orderStatusSpinner = findViewById(R.id.order_status_spinner);

        orderRecyclerView = findViewById(R.id.recycler_order_detail);
        orderRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        orderRecyclerView.setAdapter(new OrderDetailAdapter(this));

        //Get name of order user (with userPhone)
        firebaseFirestore.collection("Users").document(Common.currentOrder.getUserPhone())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                User user = document.toObject(User.class);
                                name = user.getName();
                                txt_name.setText(name);
                            } else {
                                Log.d(TAG, "No such document");
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });


        txt_order_id.setText(new StringBuilder("#").append(Common.currentOrder.getOrderId()));
        txt_phone.setText(Common.currentOrder.getUserPhone());
        txt_order_price.setText(new StringBuilder("$ ").append(Common.currentOrder.getOrderPrice()));
        txt_order_address.setText(Common.currentOrder.getOrderAddress());
        txt_order_comment.setText(Common.currentOrder.getOrderComment());



        //Set Array adapter for order status Spinner (for place data)
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                spinnerSource);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderStatusSpinner.setAdapter(spinnerArrayAdapter);

        setSpinnerSelectedBaseOnOrderStatus();



    }

    private void setSpinnerSelectedBaseOnOrderStatus() {

        switch (Common.currentOrder.getOrderStatus())
        {
            case -1:
                orderStatusSpinner.setSelection(0); //Cancelled
                break;
            case 0:
                orderStatusSpinner.setSelection(1); //Placed
                break;
            case 1:
                orderStatusSpinner.setSelection(2); //Processed
                break;
            case 2:
                orderStatusSpinner.setSelection(3); //Shipping
                break;
            case 3:
                orderStatusSpinner.setSelection(4); //Shipped
                break;
        }
    }

    //Ctrol+O
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_detail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_save_order_detail)
            saveUpdateOrder();
        return true;
    }

    private void saveUpdateOrder() {

        final int order_status = orderStatusSpinner.getSelectedItemPosition()-1;

        //Update order status
        firebaseFirestore.collection("Orders")
                .document(Common.currentOrder.getOrderId())
                .update("orderStatus", order_status)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                        Toast.makeText(ViewOrderDetailActivity.this, "Order status updated!", Toast.LENGTH_SHORT).show();
                        sendOrderUpdateNotification(Common.currentOrder, order_status);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating document", e);
                        Toast.makeText(ViewOrderDetailActivity.this, "Error de conexion", Toast.LENGTH_SHORT).show();
                    }
                });

//        compositeDisposable.add(mFCMService.updateOrderStatus(Common.currentOrder.getUserPhone(),
//                Common.currentOrder.getOrderId(),
//                order_status)
//        .observeOn(AndroidSchedulers.mainThread())
//        .subscribeOn(Schedulers.io())
//        .subscribe(new Consumer<String>() {
//            @Override
//            public void accept(String s) throws Exception {
//                sendOrderUpdateNotification(Common.currentOrder, order_status);
//            }
//        }, new Consumer<Throwable>() {
//            @Override
//            public void accept(Throwable throwable) throws Exception {
//                Log.d(TAG, "accept: ERRROR: " + throwable.getMessage());
//            }
//        }));
    }

    private void sendOrderUpdateNotification(final Order currentOrder, final int order_status) {

        firebaseFirestore.collection("Tokens").document(Common.currentOrder.getUserPhone())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            Token token = document.toObject(Token.class);
                            String userToken = token.getToken();

                            //
                            DataMessage dataMessage = new DataMessage();
                            Map<String,String> contentSend = new HashMap<>();
                            contentSend.put("title","Tu orden tiene un nuevo status:");
                            contentSend.put("message", "Orden #: " + currentOrder.getOrderId() + " ha sido actualizada a "+
                                    Common.convertCodeToStatus(order_status));
                            //Toast.makeText(ViewOrderDetailActivity.this, "TOEKN; " + userToken.getToken(), Toast.LENGTH_SHORT).show();
                            dataMessage.setTo(userToken);
                            dataMessage.setData(contentSend);
                            mFCMService.sendNotification(dataMessage)
                                    .enqueue(new Callback<MyResponse>() {
                                        @Override
                                        public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                            Log.d(TAG, "onResponse: CODE " +response.code());
                                            if(response.code() == 200) {
                                                Log.d(TAG, "onResponse: SUCCESS  " + response.body().success);
                                                if (response.body().success == 1) {
                                                    Toast.makeText(ViewOrderDetailActivity.this, "Order updated!!", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                } else {
                                                    Log.d(TAG, "onResponse: SEND FAILED");
                                                    Toast.makeText(ViewOrderDetailActivity.this, "Send notification failed !", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<MyResponse> call, Throwable t) {
                                            Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });

//        mFCMService.getToken(currentOrder.getUserPhone(),
//                "0")
//                .enqueue(new Callback<Token>() {
//                             @Override
//                             public void onResponse(Call<Token> call, Response<Token> response) {
//                                //When we have Token , just send notification to this token
//                                 Token userToken = response.body();
//                                 DataMessage dataMessage = new DataMessage();
//                                 Map<String,String> contentSend = new HashMap<>();
//                                 contentSend.put("title","Tu orden tiene un nuevo status:");
//                                 contentSend.put("message", "Orden #: " + currentOrder.getOrderId() + " ha sido actualizada a "+
//                                         Common.convertCodeToStatus(order_status));
//                                 //Toast.makeText(ViewOrderDetailActivity.this, "TOEKN; " + userToken.getToken(), Toast.LENGTH_SHORT).show();
//                                 if(userToken.getToken() != null)
//                                     dataMessage.setTo(userToken.getToken());
//                                 dataMessage.setData(contentSend);
//                                 mFCMService.sendNotification(dataMessage)
//                                     .enqueue(new Callback<MyResponse>() {
//                                         @Override
//                                         public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
//                                             Log.d(TAG, "onResponse: CODE " +response.code());
//                                             if(response.code() == 200) {
//                                                 Log.d(TAG, "onResponse: SUCCESS  " + response.body().success);
//                                                 if (response.body().success == 1) {
//                                                     Toast.makeText(ViewOrderDetailActivity.this, "Order updated!!", Toast.LENGTH_SHORT).show();
//                                                     finish();
//                                                 } else {
//                                                     Log.d(TAG, "onResponse: SEND FAILED");
//                                                     Toast.makeText(ViewOrderDetailActivity.this, "Send notification failed !", Toast.LENGTH_SHORT).show();
//                                                 }
//                                             }
//
//                                         }
//
//                                         @Override
//                                         public void onFailure(Call<MyResponse> call, Throwable t) {
//                                             Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                                         }
//                                     });
//
//                             }
//
//                             @Override
//                             public void onFailure(Call<Token> call, Throwable t) {
//                                 Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                             }
//                         });


    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}
