package com.example.nicolas.drinkfirestoreserver;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.nicolas.drinkfirestoreserver.Adapter.OrderViewAdapter;
import com.example.nicolas.drinkfirestoreserver.Model.Order;
import com.example.nicolas.drinkfirestoreserver.Retrofit.IDrinkShopAPI;
import com.example.nicolas.drinkfirestoreserver.Utils.Common;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ShowOrderActivity extends AppCompatActivity {
    private static final String TAG = "ShowOrderActivity";

    IDrinkShopAPI mService;
    RecyclerView recyclerView;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    BottomNavigationView navigationView;

    private FirebaseFirestore firebaseFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_order);

        mService = Common.getAPI();

        firebaseFirestore = FirebaseFirestore.getInstance();

        recyclerView = findViewById(R.id.recycler_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        navigationView = findViewById(R.id.bottom_navigation);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.order_new) {
                    loadAllOrders(0);
                }
                else if (item.getItemId() == R.id.order_cancel) {
                    loadAllOrders(-1);
                }
                else if (item.getItemId() == R.id.order_processing) {
                    loadAllOrders(1);
                }
                else if (item.getItemId() == R.id.order_shipping) {
                    loadAllOrders(2);
                }
                else if (item.getItemId() == R.id.order_shipped) {
                    loadAllOrders(3);
                }
                return true;
            }
        });
        loadAllOrders(0);
    }

    private void loadAllOrders(int statusCode) {

        final List<Order> orders = new ArrayList<>();

        firebaseFirestore.collection("Orders")
                .whereEqualTo("orderStatus",statusCode)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                Order order = document.toObject(Order.class).withId(document.getId());
                                orders.add(order);
                                //Toast.makeText(ShowOrderActivity.this, "Encontrado: " + document.get("orderAddress"), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        Toast.makeText(ShowOrderActivity.this, "orders size " + orders.size(), Toast.LENGTH_SHORT).show();
                        displayOrders(orders);
                    }
                });
    }

    private void displayOrders(List<Order> orders) {
        Collections.reverse(orders); //show order for newest first
        OrderViewAdapter adapter = new OrderViewAdapter(this,orders);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadAllOrders(0);
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}
